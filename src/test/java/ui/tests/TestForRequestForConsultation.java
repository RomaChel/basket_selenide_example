package ui.tests;

import ui.base_package.BaseTest;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ui.hillel_site.page_object.ManePage;
import ui.utils.Listener;

import static com.codeborne.selenide.Selenide.open;
import static ui.utils.CommonMethods.randomMail;

@Listeners(Listener.class)
public class TestForRequestForConsultation extends BaseTest {

    ManePage manePage = new ManePage();

    @Test
    @Owner(value = "Chelombitko")
    @Description(value = "Відправляе запит на консультацію")
    public void requestForConsultation() {
        open("https://ithillel.ua/");
        manePage.clickByConsultationBnt()
                .filingInputName("Autotest")
                .filingInputMail(randomMail())
                .choosesTelCode("ad")
                .filingInputTelNumber("123456")
                .choosesCourse("Java Pro")
                .enterComment("This is autotest")
                .clickPrivacyCheckBox()
                .clickBySubmitBtn()
                .checksResultText();
    }
}
