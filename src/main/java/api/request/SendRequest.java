package api.request;

import api.pojo.body_request.JiraIssueRequest;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class SendRequest {

    public static Response sendPostRequest(RequestSpecification recSpec, JiraIssueRequest jiraIssueRequest, String enpoint,
                                            int expectedStatusCod) {
        return given()
                .spec(recSpec)
                .when()
                .body(jiraIssueRequest)
                .log().all()
                .post(enpoint)
                .then()
                .log().all()
                .statusCode(expectedStatusCod)
                .extract().response();

    }
}
